---
title: How to use Let's Encrypt on Dreamhost shared hosting
tags: tutorials
author: Rob Nugen
date: 2015-12-06T21:20:00+09:00
---

(pull requests for these instructions are welcome at https://bitbucket.org/thunderrabbit/use-letsencrypt-on-dreamhost)

Yay!  [Let's Encrypt entered public beta](https://letsencrypt.org/2015/12/03/entering-public-beta.html) a few days ago.  Let's see if we can do it on Dreamhost.

(Updated)

If you already have a domain:

Step 1: Select your domain and the checkbox on [Add Secure Hosting](https://panel.dreamhost.com/index.cgi?tree=domain.secure&current_step=Index&next_step=Add)

the end.


If you are registering a domain:

Step 1: Select the HTTPS checkbox

the end.
